<?php

class Product {
    private $id;
    private $name;
    private $price;

    public function __construct($i, $n, $p) {
        $this->id = $i;
        $this->name = $n;
        $this->price = $p;
    }

    public function getIdProduct() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getPrice() {
        return $this->price;
    }
}

?>