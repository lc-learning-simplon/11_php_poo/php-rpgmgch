<?php

    class Client extends User {

        private $billAmount = 0;
        private $cart = [];
        
        public function upgradeBillAmount($b) {
            $this->billAmount += $b;
        }
        
        public function addProductToCart($product) {
            array_push($this->cart, $product);
        }
        
        public function getBillAmount() {
            return $this->billAmount;
        }
    
        public function getCart() {
            return $this->cart;
        }

        public function buy($product) {
            $this->upgradeBillAmount($product->getPrice());
            $this->addProductToCart($product->getName());
        }
    }

?>