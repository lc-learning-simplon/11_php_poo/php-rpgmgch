<?php

class Vegetable extends Product {
    private $productorName;
    private $expiresAt;

    public function isFresh() {
        if((date_diff($this->expiresAt, date_create()))->format('%R%a days') > 0) {
            return false;
        }
        else {
            return true;
        }
    }

    public function __construct($i, $n, $p, $pn, $d) {
        parent::__construct($i, $n, $p);
        $this->productorName = $pn;
        $this->expiresAt = date_create($d); //("dd-mm-yyyy")
    }

    public function getProductorName() {
        return $this->productorName;
    }

    public function getExpiresAt() {
        return ($this->expiresAt)->Format("d-m-Y");
    }
}

?>