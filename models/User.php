<?php

    class User {
        private $id;
        private $email;
        private $createdAt;

        function __construct($i, $m) {
            $this->id = $i;
            $this->email = $m;
            $this->createdAt = date("d") . "-" . date("m") . "-" . date("Y");
        }

        function getId() {
            return $this->id;
        }

        function getEmail() {
            return $this->email;
        }

        function getDate() {
            return $this->createdAt;
        }

    }


?>