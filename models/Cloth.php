<?php

class Cloth extends Product {
    private $brand;

    public function __construct($i, $n, $p, $b) {
        parent::__construct($i, $n, $p);
        $this->brand = $b;
    }

    public function try() {
        
    }

    public function getBrand() {
        return $this->brand;
    }
}

?>