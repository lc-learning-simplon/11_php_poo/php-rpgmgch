<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Users Table</title>
</head>
<body>

<?php

$users = require(__DIR__ ."/../data/users.php");

echo "<table>
        <tr>
            <th>Client ID |</th>
            <th>Email |</th>
            <th>Date</th>
        </tr>";

foreach($users as $value) {
    echo "<tr>
                <th>" . $value->getId() .  "</th>
                <th>" . $value->getEmail() .  "</th>
                <th>" . $value->getDate() .  "</th>
    </tr>";
}

echo "</table>";


?>
    
</body>
</html>