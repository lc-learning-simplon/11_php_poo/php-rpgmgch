<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Products Table</title>
</head>
<body>

<?php

$products = require(__DIR__ ."/../data/products.php");

echo "<table>
        <tr>
            <th>Product ID |</th>
            <th>Name |</th>
            <th>Price[$] |</th>
            <th>Productor's name |</th>
            <th>Expire date |</th>
            <th>Brand</th>
        </tr>";

foreach($products as $value) { 
?>
        <tr>
                    <th><?= $value->getIdProduct()?></th>
                    <th><?= $value->getName()?></th>
                    <th><?= $value->getPrice()?></th>
                    <th><?= method_exists($value, "getProductorName") ? $value->getProductorName() : "-";?></th>
                    <th><?= method_exists($value, "getExpiresAt") ? $value->getExpiresAt() : "-";?></th>
                    <th><?= method_exists($value, "getBrand") ? $value->getBrand() : "-";?></th>
        </tr>
<?php
}

echo "</table>";

// echo "<table>
//         <tr>
//             <th>Product ID</th>
//             <th>Name</th>
//             <th>Price[$]</th>
//             <th>Brand</th>
//         </tr>";

// foreach($products as $value) {
//     if(is_string($value->getBrand())) {
//         echo "<tr>
//                     <th>" . $value->getIdProduct() .  "</th>
//                     <th>" . $value->getName() .  "</th>
//                     <th>" . $value->getPrice() .  "</th>
//                     <th>" . $value->getBrand() .  "</th>
//         </tr>";
//     }
// }

// echo "</table>";


?>
    
</body>
</html>