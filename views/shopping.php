<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shopping Time !</title>
</head>
<body>
    <?php
        $users = require(__DIR__."/../data/users.php");
        $products = require(__DIR__."/../data/products.php");
    ?>
    <form action="/validateOrder.php" method="POST">
        <select name="user" id="user">
            <?php
                foreach($users as $key => $user) {
                        echo "<option>" . $key ."</option>";
                }
            ?>
        </select>

        <select name="product1" id="product1">
            <?php
                foreach($products as $key => $product) {
                        echo "<option>" . $key ."</option>";
                }
            ?>
        </select>

        <select name="product2" id="product2">
            <?php
                foreach($products as $key => $product) {
                        echo "<option>" . $key ."</option>";
                }
            ?>
        </select>

        <select name="product3" id="product3">
            <?php
                foreach($products as $key => $product) {
                        echo "<option>" . $key ."</option>";
                }
            ?>
        </select>
        
        <button type="submit">Choose</button>
    </form>
</body>
</html>