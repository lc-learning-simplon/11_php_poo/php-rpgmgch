<?php

$users = require("data/users.php");
$product = require("data/products.php");

// echo $users[0]->getEmail();
// echo $product[0]->getName();
// echo $product[0]->getPrice();

$client1->buy($product[0]);
$client2->buy($product[1]);
$client2->buy($product[2]);

echo "Cart Client1 : ";

foreach($client1->getCart() as $key => $value) {
    if($key+1 == count($client1->getCart())) {
        echo $value;
    }
    else {
        echo $value . ", ";
    }
} 

echo ". Total : " . $client1->getBillAmount() . " $.<br>";

echo "Cart Client2 : ";

foreach($client2->getCart() as $key => $value) {
    if($key+1 == count($client2->getCart())) {
        echo $value;
    }
    else {
        echo $value . ", ";
    }
} 

echo ". Total : " . $client2->getBillAmount() . " $.";

?>