<?php

require "models/User.php";
require "models/Client.php";

$client1 = new Client(1, "client1@mail.com");
$client2 = new Client(2, "client2@mail.com");

return [
    "Client1" => $client1,
    "Client2" => $client2
];

?>