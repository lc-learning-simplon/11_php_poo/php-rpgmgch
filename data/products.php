<?php

require "models/Product.php";
require "models/Vegetable.php";
require "models/Cloth.php";

$tomato = new Vegetable(1, "Tomato", "2", "Michel", "30-10-2019");
$carrot = new Vegetable(2, "Carrot", "3", "Jean", "31-10-2019");
$sweet = new Cloth(3, "Sweet", "30", "XXL");
$pant = new Cloth(4, "Pant", "23", "L");
$tshirt = new Cloth(5, "T-Shirt", "10", "XS");

return [
    "tomato" => $tomato,
    "carrot" => $carrot,
    "sweet" => $sweet,
    "pant" => $pant,
    "tshirt" => $tshirt
];

?>